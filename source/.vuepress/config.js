module.exports = {
	base: '/viktoriianne-blog/',
	host: "172.22.41.151",
	dest: 'dist',
	title: 'Viktoii Anne\'s Blog',
	description: '',
	themeConfig: {
		search: false,
		nav: [
			{
				text: 'Home',
				link: '/'
			},
			{
				text: 'Blog',
				link: '/blog.html'
			}
		]
	}
}
